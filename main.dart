import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:nails_app/addAppointment.dart';
import 'package:flutter/cupertino.dart';
import 'package:nails_app/sessionList.dart';

Future main() async
{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: FirebaseOptions(
      apiKey: "AIzaSyCG9JkenwnfQBntdc65cakEzoByDgR3704",
      authDomain: "vunene-9be15.firebaseapp.com",
      projectId: "vunene-9be15",
      storageBucket: "vunene-9be15.appspot.com",
      messagingSenderId: "228267939639",
      appId: "1:228267939639:web:c07bf122b6bfce34316cb2"
    ));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context)
  {
    return MaterialApp
      (
      title: 'Nails Appointment',
      theme: ThemeData(
        appBarTheme: const AppBarTheme
          (
            backgroundColor:Color(0xFFF3E5F5),
          centerTitle:true
        ),
        textTheme: const  TextTheme(bodyText1: TextStyle(color:Colors.black))
      ),
      home:Scaffold(
        appBar: AppBar(title:Text('Nails Appointment'),),
          body:Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/images/nails.png',
                      height:220,
                        width:220
                    ),
                    AddAppointment(),
                    sessionList()
                  ]
              )
          )
    );
  }
}


