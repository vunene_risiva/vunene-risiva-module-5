import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:nails_app/addAppointment.dart';
import 'dart:math';

class sessionList extends StatefulWidget
{
  const sessionList({Key? key}) : super(key: key);


  @override
  State<sessionList> createState() => _sessionListState();
}

class _sessionListState extends State<sessionList>
{
  final Stream<QuerySnapshot> _myAppointments=
    FirebaseFirestore.instance.collection("NailAppointment").snapshots();

  @override
  Widget build(BuildContext context)
  {
    return StreamBuilder(stream: _myAppointments,builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Object?>>snapshot)
    {
      if(snapshot.hasError){
        return Text("Something went wrong");
      }

      if(snapshot.connectionState== ConnectionState.waiting)
        {
          return CircularProgressIndicator();
        }

      return Row(
        children: [
          Expanded(child: SizedBox(
              height: (MediaQuery.of(context).size.height),
              width: (MediaQuery.of(context).size.width),
              child: ListView(
                children: snapshot.data!.docs.map((DocumentSnapshot documentSnapshot){
                  Map<String,dynamic> data = documentSnapshot.data()! as Map<String,dynamic>;
                  return Column(
                      children:[
                        Card(
                            child: Column(
                                children:[
                                  ListTile(
                                    title: Text(data["Client Name"]),
                                    subtitle: Text(data["appointment"]),
                                  )

                                ]
                            )
                        )
                      ]
                  );
                }).toList().cast(),

              )
          ))
        ],
      );
    });

  }
}

// children: snapshot.data!.docs.map((DocumentSnapshot document) {
// Map<String, dynamic> data= document.data()! as Map<String, dynamic>;
// return ListTile(
// title: Text(data['name']),
// subtitle: Text(data['appointment'])
// );
// }).toList().cast() ,



