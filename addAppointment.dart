import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:nails_app/sessionList.dart';

class AddAppointment extends StatefulWidget {
  const AddAppointment({Key? key}) : super(key: key);

  @override
  State<AddAppointment> createState() => _AddAppointmentState();
}

class _AddAppointmentState extends State<AddAppointment> {
  @override


  Widget build(BuildContext context)
  {

    TextEditingController nameController= TextEditingController();
    TextEditingController appointmentController= TextEditingController();
    TextEditingController ageController= TextEditingController();

    AddAppointment() {
      final name = nameController.text;
      final appointment = appointmentController.text;
      final age = ageController.text;

      final ref = FirebaseFirestore.instance.collection("NailAppointment")
          .doc();

      return ref.set({
        "Client Name": name,
      "appointment": appointment,
      "age": age,
    "doc_id":ref.id}).then((value) => null).catchError((onError)=>log(onError));
    }
    return Column(
      children: [Column(children:[
        Padding(padding: EdgeInsets.symmetric(horizontal: 8,vertical:8)),
          TextField( controller:nameController, decoration: InputDecoration( border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),hintText: "Enter client name")),

        Padding(padding: EdgeInsets.symmetric(horizontal: 8,vertical:8)),
        TextField( controller:ageController, decoration: InputDecoration( border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),hintText: "Enter client age")),

        Padding(padding: EdgeInsets.symmetric(horizontal: 8,vertical:8)),
          TextField(controller: appointmentController, decoration: InputDecoration( border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),hintText: "Enter appointment day")),
        ElevatedButton(onPressed: (){AddAppointment();}, child: Text("Add appointment")),
        //ElevatedButton(onPressed: (){sessionList();}, child: Text("Show sessions"))
      ])],
    );
    sessionList();
  }
}


